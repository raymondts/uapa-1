from django.db import models


class Producto(models.Model):
    CATEGORY = {
        ('Bebidas', 'Bebidas'),
        ('Frutas', 'Frutas')

    }

    name = models.CharField(verbose_name='Nombre', max_length=30)
    category = models.CharField(choices=CATEGORY, default='Frutas', max_length=30)
    price = models.DecimalField(decimal_places=2,max_digits=20)
    quantity = models.PositiveIntegerField()

    def __str__(self):
        return self.name